﻿using UnityEngine;
using System.Collections;
using MidiJack;
using ParticlePlayground;

public class MidiInVisualiser : MonoBehaviour {

    PlaygroundParticlesC particles;

    void NoteOn(MidiChannel channel, int note, float velocity)
    {
        particles = gameObject.GetComponent<PlaygroundParticlesC>();


        particles.Emit(
                       Mathf.RoundToInt(velocity*100 * Time.deltaTime),
                       new Vector3(note%12*1.5f, (note/12+.2f)*2f, 0f),
                       new Vector3(note%12*1.5f+1f*velocity, note/12*2f, 0f),
                       new Vector3(1f*velocity, 1f*velocity, -1f*velocity),
                       new Vector3(-1f*velocity, -1f*velocity, -1f*velocity),
                       Color.white
                       );
        


    }




    void OnEnable()
    {
        MidiMaster.noteOnDelegate += NoteOn;
 
    }

    void Start()
    {
        particles = GetComponent<PlaygroundParticlesC>();
    }
    


    void OnDisable()
    {
        MidiMaster.noteOnDelegate -= NoteOn;

    }
}
