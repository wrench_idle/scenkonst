﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;

[AddComponentMenu("Cocuy/Paint Attacher")]
public class PaintAttacher : MonoBehaviour
{
    public GameObject[] PaintPoint;
    private GameObject[] myBrick;

    private Vector3[] m_previousPosition;
    private Vector3[] m_initPosition;

    [HideInInspector]
    public float m_velocityStrength = 10f;
    [HideInInspector]
    public float m_velocityRadius = 5f;

    [HideInInspector]
    public float m_particlesStrength = 1f;
    [HideInInspector]
    public float m_particlesRadius = 5f;

    public FluidSimulator m_fluid;
    public ParticlesArea m_particlesArea;
    public bool m_alwaysOn = true;
    private bool[] first;

    public int pIndex;
    public int points;

    public GameObject brick;

    PlaygroundParticlesC particles;
    public float[] velocity;
    public float totalVelocity;

    public float[] avarageSpeed;
    public float[] topSpeed;
    float[][] speedCount;
    float[] speedSum;
    float[] releaseTimer;
    int[] speedFrame;

    void Start()
    {
        m_previousPosition = new Vector3[PaintPoint.Length];
        m_initPosition = new Vector3[PaintPoint.Length];
        velocity = new float[PaintPoint.Length];
        avarageSpeed = new float[PaintPoint.Length];
        topSpeed = new float[PaintPoint.Length];
        speedCount = new float[PaintPoint.Length][];
        speedSum = new float[PaintPoint.Length];
        releaseTimer = new float[PaintPoint.Length];
        speedFrame = new int[PaintPoint.Length];
        myBrick = new GameObject[PaintPoint.Length];
        first = new bool[PaintPoint.Length];

        pIndex = GetComponent<CubemanController>().playerIndex;

        for (int i = 0; i < PaintPoint.Length; i++)
        {
            myBrick[i] = Instantiate(brick, PaintPoint[i].transform.position, PaintPoint[i].transform.rotation) as GameObject;
            //myBrick[i].transform.parent = gameObject.transform;
            m_previousPosition[i] = PaintPoint[i].transform.position;
            m_initPosition[i] = PaintPoint[i].transform.position;
            speedCount[i] = new float[40];
            first[i] = true;


        }
        particles = GetComponent<PlaygroundParticlesC>();


    }

    void LateUpdate()
    {
        float total = 0;
        KinectManager manager = KinectManager.Instance;
        int users = manager.GetUsersCount();

        //if (limiter >= gameObject.GetComponent<CubemanController>().playerIndex + 1)
        //{

        

        switch (users)
        {
            case 1:
                switch (pIndex)
                {
                    default:
                        points = 0;
                        break;
                }
                break;
            case 2:
                switch (pIndex)
                {
                    default:
                        points = 1;
                        break;
                 }
                break;
            default:
                switch (pIndex)
                {
                    default:
                        points = 2;
                        break;
                    case 2:
                        points = 3;
                        break;
                }
                break;
        }
        

        for (int i = 0; i < (PaintPoint.Length)-points; i++)
        {
            Vector3 lerpPos;
            if (PaintPoint[i].activeSelf)
            {
                myBrick[i].transform.position = PaintPoint[i].transform.position;
                if (first[i])
                {
                    m_previousPosition[i] = PaintPoint[i].transform.position;
                    first[i] = false;
                    lerpPos = PaintPoint[i].transform.position;
                    velocity[i] = 0;
                }
                else
                {
                    lerpPos = Vector3.Lerp(m_previousPosition[i], PaintPoint[i].transform.position, 0.5f);
                    velocity[i] = ((lerpPos - m_previousPosition[i]).magnitude / Time.deltaTime);
                }

                releaseTimer[i] = 0;
                //m_initPosition[i] = PaintPoint[i].transform.position;


                if (velocity[i] > 20)
                {
                    velocity[i] = 0;
                }



            }
            else
            {
                lerpPos = PaintPoint[i].transform.position;
                if (releaseTimer[i] > 20)
                {
                    first[i] = true;

                    //m_previousPosition[i] = PaintPoint[i].transform.position;
                    velocity[i] = 0;
                    //releaseTimer[i] = 0;
                }
                else
                {
                    releaseTimer[i]++;
                }

            }

            speedCount[i][speedFrame[i] % speedCount[i].Length] = velocity[i];

            speedFrame[i]++;

            speedSum[i] = SumArray(speedCount[i]);

            topSpeed[i] = Mathf.Max(speedCount[i]);

            avarageSpeed[i] = speedSum[i] / speedCount[i].Length;

            //total += velocity[i];

            //Ray ray = new Ray(lerpPos,new Vector3(0,0,1));
            Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(lerpPos));


            RaycastHit hitInfo = new RaycastHit();
            if (m_particlesArea.GetComponent<Collider>().Raycast(ray, out hitInfo, 100))
            {

                float fWidth = m_particlesArea.GetComponent<Renderer>().bounds.extents.x * 2f;
                float fRadius = (m_particlesRadius * m_particlesArea.GetWidth()) / fWidth;
                m_particlesArea.AddParticles(hitInfo.textureCoord, fRadius * (velocity[i] / 3.3f), m_particlesStrength * (velocity[i]) * Time.deltaTime);

            }
            

            if (m_fluid.GetComponent<Collider>().Raycast(ray, out hitInfo, 100))
            {
                Vector3 direction = (lerpPos - m_previousPosition[i]) * m_velocityStrength * (velocity[i] / 2) * Time.deltaTime;
                float fWidth = m_fluid.GetComponent<Renderer>().bounds.extents.x * 2f;
                float fRadius = (m_velocityRadius * m_fluid.GetWidth()) / fWidth;
                m_fluid.AddVelocity(hitInfo.textureCoord, direction, fRadius);
            }



            m_previousPosition[i] = lerpPos;

            //}

            totalVelocity = (totalVelocity * 0.8f) + (total * 0.2f);

            // Debug.Log(total);
        }
    }

    public float SumArray(float[] toBeSummed)
    {
        int sum = 0;
        foreach (int item in toBeSummed)
        {
            sum += item;
        }
        return sum;
    }


}

