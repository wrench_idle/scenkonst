﻿using UnityEngine;
using System.Collections;
using DigitalRuby.Tween;
using Midi;

public class PauseMode : MonoBehaviour
{
    private KinectRecorderPlayer playr;
    // Use this for initialization
    public Camera came;
    private int sceneNum = 0;
    public bool isNew = true;
    private GameObject Fluid;
    public GameObject Circle;
    private float timer;

    public bool bAutoChange =true;
    bool someone;
    float danceTimer;

    OutputDevice outputDevice;

    void Start()
    {

        came = Camera.main;
        playr = KinectRecorderPlayer.Instance;
        outputDevice = GameObject.Find("Midi").GetComponent<MidiOut>().outputDevice;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        int count = playr.manager.GetUsersCount();
        someone = GameObject.Find("Kinect").GetComponent<PlayerDetectorController>().bUserFound;
        //Debug.Log(someone);
        if (!someone)
        {
            if (isNew)
            {
                SceneChange();   
            }
            danceTimer = (Time.realtimeSinceStartup);
        }
        else
        {
            isNew = true;
            if (Time.realtimeSinceStartup - danceTimer > 180)
            {
                if (bAutoChange){
                SceneChange();
                danceTimer = (Time.realtimeSinceStartup);
                 }
            }
        }



        //else if (count == 2)
        //{
        //    if (playr)
        //    {
        //        if (playr.IsPlaying())
        //        {
        //            playr.StopRecordingOrPlaying();
        //        }
        //    }
        //}




    }

    void SceneChange()
    {
        TweenColor(sceneNum);
        sceneNum = ((sceneNum + 1) % 3);
        isNew = false;
        TweenMove(sceneNum);
        outputDevice = GameObject.Find("Midi").GetComponent<MidiOut>().outputDevice;
        outputDevice.SendProgramChange(0, (Instrument)((sceneNum+3)%3));
    }

    private void TweenColor(int g)
    {
        Fluid = GameObject.Find("Cocuy_Fluid");
        // Color endColor = UnityEngine.Random.ColorHSV(0.0f, 1.0f, 0.0f, 1.0f, 0.5f, 1.0f, 1.0f, 1.0f);
        int n = mod(g - 1, 3);

        came.gameObject.Tween("ColorChange", 1.0f, 0.0f, 12.0f, TweenScaleFunctions.QuadraticEaseOut, (t) =>
        {
            // progress
            came.GetComponents<AmplifyColorEffect>()[g].BlendAmount = t.CurrentValue;
        }, (t) =>
        {
            // completion
        });

        came.gameObject.Tween("ColorChange2", 0.0f, 1.0f, 6.0f, TweenScaleFunctions.QuadraticEaseOut, (t) =>
        {
            // progress

            came.GetComponents<AmplifyColorEffect>()[n].BlendAmount = t.CurrentValue;
        }, (t) =>
        {
            // completion
        });

        switch (g)
        {
            case 2:
                Fluid.GetComponent<ParticlesArea>().Dissipation = 0.986f;
                Fluid.GetComponent<FluidSimulator>().Vorticity = 30f;
                Fluid.GetComponent<FluidSimulator>().Speed = 500f;
                break;
            case 11:
                Fluid.GetComponent<ParticlesArea>().Dissipation = 0.996f;
                Fluid.GetComponent<FluidSimulator>().Vorticity = 0f;
                Fluid.GetComponent<FluidSimulator>().Speed = 500f;
                break;
            case 0:
                Fluid.GetComponent<ParticlesArea>().Dissipation = 0.976f;
                Fluid.GetComponent<FluidSimulator>().Vorticity = 20f;
                Fluid.GetComponent<FluidSimulator>().Speed = 1000f;
                break;
            case 1:
                Fluid.GetComponent<ParticlesArea>().Dissipation = 0.946f;
                Fluid.GetComponent<FluidSimulator>().Vorticity = 0f;
                Fluid.GetComponent<FluidSimulator>().Speed = 1000f;
                break;
            case 20:
                Fluid.GetComponent<ParticlesArea>().Dissipation = 0.976f;
                Fluid.GetComponent<FluidSimulator>().Vorticity = 0f;
                Fluid.GetComponent<FluidSimulator>().Speed = 200f;
                break;
        
    }

    }

    private void TweenMove(int g)
    {

        Vector3 startPos;
        Vector3 endPos;

        switch(g)
        {
            case 0:
                startPos = new Vector3(0, 7, 10);
                endPos = new Vector3(0, -8.5f, 10.5f);
                break;
            case 2:
                startPos = new Vector3(15, 7, 10);
                endPos = new Vector3(-16.5f, -8.5f, 10.5f);
                break;
            case 3:
                startPos = new Vector3(0, -7, 10);
                endPos = new Vector3(0, 8.5f, 10.5f);
                break;
            default:
                startPos = new Vector3(-15, 7, 10);
                endPos = new Vector3(16.5f, -8.5f, 10.5f);
                break;

        }

        

        Circle.gameObject.Tween("MoveCircle", startPos, endPos, 1.75f, TweenScaleFunctions.Linear, (t2) =>
        {
                // progress
                Circle.gameObject.transform.position = t2.CurrentValue;
        }, (t2) =>
        {
                // completion



            });

    }

    private int mod(int k, int n) { return ((k %= n) < 0) ? k + n : k; }
}
