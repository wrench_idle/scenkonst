﻿using UnityEngine;
using System.Collections;
using DigitalRuby.Tween;

public class TextMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Sizing();
        Roating();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void Sizing()
    {
        gameObject.Tween("sizing", 130, 100, 0.5f, TweenScaleFunctions.QuadraticEaseOut, (t) =>
        {
            // progress

            gameObject.transform.position = new Vector3(0.5f, 0.5f, t.CurrentValue);
        }, (t) =>
        {
            gameObject.Tween("sizing", 100, 130, 0.5f, TweenScaleFunctions.QuadraticEaseOut, (r) =>
            {
                // progress

                gameObject.transform.position = new Vector3(0.5f, 0.5f, r.CurrentValue);
            }, (r) =>
            {
                Sizing();
            });
        });

    }

    void Roating()
    {
        gameObject.Tween("rotating", 0.1f, 0.25f, 5f, TweenScaleFunctions.Linear, (t) =>
        {
            // progress
            gameObject.transform.Rotate(0,-t.CurrentValue,0);

        }, (t) =>
        {
            gameObject.Tween("rotating", 0.25f, 0.1f, 5f, TweenScaleFunctions.Linear, (r) =>
            {
                // progress

                gameObject.transform.Rotate(0, r.CurrentValue, 0);
            }, (r) =>
            {
                Roating();
            });
        });

    }
}
