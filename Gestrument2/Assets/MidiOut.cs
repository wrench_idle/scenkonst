﻿using UnityEngine;
using System.Collections;
using Midi; // needs this

public class MidiOut : MonoBehaviour {

    public OutputDevice outputDevice;
    public int outDevice;


    // Use this for initialization
    void Start()
    {

        outputDevice = OutputDevice.InstalledDevices[outDevice];
        if (outputDevice.IsOpen) outputDevice.Close();
        if (!outputDevice.IsOpen) outputDevice.Open();

        foreach (OutputDevice device in OutputDevice.InstalledDevices)
        {
            Debug.Log(device.Name);
        };
        Debug.Log("USING" +outputDevice.Name);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void changeOut (int outNum) {
        if (outputDevice != null)
        {
            outputDevice.SilenceAllNotes();
           outputDevice.Close();
            OutputDevice.UpdateInstalledDevices();
            outputDevice = OutputDevice.InstalledDevices[outNum];
            
            if (!outputDevice.IsOpen) outputDevice.Open();
            Debug.Log("Jaha " + outputDevice.Name);
        }  


        return;
    }

        void OnDestroy()
    {
            if (outputDevice != null && outputDevice.IsOpen)
            {
                outputDevice.SilenceAllNotes();
                outputDevice.Close();
            }
        }

        void OnDisable()
    {
            if (outputDevice != null && outputDevice.IsOpen) outputDevice.Close();
        }

    }


