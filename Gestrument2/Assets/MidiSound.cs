﻿// usage: copy Midi.dll to Assets/Plugins/ folder
// Get it from https://code.google.com/p/midi-dot-net/


using UnityEngine;
using System.Collections;
using Midi; // needs this
using CielaSpike;
using System.Threading;



public class MidiSound : MonoBehaviour
{
    OutputDevice outputDevice;


    public int outChannel = 1;
    public int outCCX = 20;
    public int outCCY = 21;
    public int outCCP = 22;
    public int outCCV = 7;

    public float minX = 0.1f;
    public float maxX = 4;
    public float minY = -3;
    public float maxY = 3;

    public int id = 0;

    GameObject[] PaintObject;
    public float[] velocity;
    float[] averagVel;
    bool[] firstOnset;
    float[] timer;
    float[] prevVel;

    int points;

    bool isRunning = false;

    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }


    void Start()
    {


        //PaintObject =Paint.GetComponent<PaintAttacher>().PaintPoint;
        // velocity = Paint.GetComponent<PaintAttacher>().velocity;

        PaintObject = GetComponent<PaintAttacher>().PaintPoint;
        velocity = GetComponent<PaintAttacher>().velocity;
        prevVel = new float[PaintObject.Length];
        averagVel = GetComponent<PaintAttacher>().avarageSpeed;
        firstOnset = new bool[PaintObject.Length];
        timer = new float[PaintObject.Length];

        
        outputDevice = GameObject.Find("Midi").GetComponent<MidiOut>().outputDevice;
        //outputDevice = OutputDevice.InstalledDevices[4];
        //Debug.Log(" " + outputDevice.Name);
        //if (outputDevice.IsOpen) outputDevice.Close();
        //if (!outputDevice.IsOpen) outputDevice.Open();
        StartCoroutine(ninjaMidi());
        // play note
        //outputDevice.SendNoteOn(Channel.Channel1, Pitch.A6, 80);  // Middle C, velocity 80
        //outputDevice.SendPitchBend(Channel.Channel1, 7000);  // 8192 is centered, so 7000 is bent down
    }

    void Update()
    {
        PaintObject = GetComponent<PaintAttacher>().PaintPoint;
        velocity = GetComponent<PaintAttacher>().velocity;
        averagVel = GetComponent<PaintAttacher>().avarageSpeed;
        outputDevice = GameObject.Find("Midi").GetComponent<MidiOut>().outputDevice;
        points = GetComponent<PaintAttacher>().points;

        KinectManager manager = KinectManager.Instance;
        int users = manager.GetUsersCount();

        switch (users)
        {
            case 1:
                switch (id)
                {
                    case 1:
                        outChannel = 1;
                        break;
                    case 2:
                        outChannel = 6;
                        break;
                    case 3:
                        outChannel = 12;
                        break;
                }
                break;
            case 2:
                switch (id)
                {
                    case 1:
                        outChannel = 1;
                        break;
                    case 2:
                        outChannel = 5;
                        break;
                }
                break;
            default:
                switch (id)
                {
                    case 1:
                        outChannel = 1;
                        break;
                    case 2:
                        outChannel = 4;
                        break;
                    case 3:
                        outChannel = 7;
                        break;
                }
                break;
                
        }


        if (!isRunning)
        {

        };

        //if (outputDevice != null && outputDevice.IsOpen)
        //{

        //    for (int i = 0; i < PaintObject.Length; i++)
        //    {

        //        if (velocity[i] > 1)
        //        {
        //            float x = Mathf.Clamp(PaintObject[i].transform.position.x, minX, maxX);
        //            float y = Mathf.Clamp(PaintObject[i].transform.position.y, minY, maxY);
        //            float z = Mathf.Clamp(velocity[i], 0, 10f);
        //            //Debug.Log("Test" + x + " " + y);
        //            x = map(x, minX, maxX, 0, 127);
        //            y = map(y, minY, maxY, 0, 127);
        //            z = map(z, 0, 10f, 0, 127);
        //            //Debug.Log("Test" + i);
        //            outputDevice.SendControlChange(outChannel - 1 + i, outCCX, (int)x);
        //            outputDevice.SendControlChange(outChannel - 1 + i, outCCY, (int)y);


        //            outputDevice.SendControlChange(outChannel - 1 + i, outCCV, (int)z);
        //            outputDevice.SendControlChange(outChannel - 1 + i, outCCP, 127);

        //            //Debug.Log(z);
        //        }
        //        else
        //        {
        //            outputDevice.SendControlChange(outChannel - 1 + i, outCCP, 0);
        //        }

        //        //Debug.Log("Test" + i);

        //    }



        //}
    }

    IEnumerator ninjaMidi()
    {

        Task task;
        this.StartCoroutineAsync(mymidiUpdate(), out task);
        yield return StartCoroutine(task.Wait());
              
        StartCoroutine(ninjaMidi());

    }

    IEnumerator mymidiUpdate()
    {

        //yield return Ninja.JumpToUnity;
        //Debug.Log("yo");
        //yield return Ninja.JumpBack;
        if (outputDevice != null && outputDevice.IsOpen)
        {

            for (int i = 0; i < PaintObject.Length-points; i++)
            {

                if (velocity[i] > 0.2)
                {
                    yield return Ninja.JumpToUnity;
                    
                    //float x = Mathf.Clamp(PaintObject[i].transform.position.x, minX, maxX);
                    float x = Mathf.Clamp(averagVel[i], minX, maxX);
                    float y = Mathf.Clamp(PaintObject[i].transform.position.y, minY, maxY);
                    float z = Mathf.Clamp(GetComponent<PaintAttacher>().topSpeed[i], 0, 10f);
                    yield return Ninja.JumpBack;
                  
                    x = map(x, minX, maxX, 0, 100);
                    y = map(y, minY, maxY, 0, 127);
                    z = map(z, 0, 10f, 0, 127);

                    x = x * 0.1f + prevVel[i] * 0.9f;
                   // if (timer[i]<10 && firstOnset[i])
                    //{
                    //    outputDevice.SendControlChange(outChannel - 1 + i, outCCX, 127);
                    //   // firstOnset[i] = false;
                        
                    //}
                    //else
                    //{
                        
                            outputDevice.SendControlChange(outChannel - 1 + i, outCCX, (int)x);
                    
                   // }
                
                    //Debug.Log(x);

                    outputDevice.SendControlChange(outChannel - 1 + i, outCCY, (int)y); 
                    outputDevice.SendControlChange(outChannel - 1 + i, outCCV, (int)z);

                    if (timer[i] > 5)
                    {
                        outputDevice.SendControlChange(outChannel - 1 + i, outCCP, 127);
                        firstOnset[i] = false;
                    }
                    // yield return Ninja.JumpToUnity;
                    //Debug.Log(velocity[i]);
                   timer[i]++;
                    prevVel[i] = x;
                }
                else if (velocity[i] < 0.05)
                {
                    outputDevice.SendControlChange(outChannel - 1 + i, outCCP, 0);
                    firstOnset[i] = true;
                    timer[i] = 0;
                    prevVel[i] = 0;
                }
                //yield return Ninja.JumpToUnity;
                //Debug.Log("Test" + i);
                //yield return Ninja.JumpBack;
            }
        
            yield break;

        }
        //  yield return Ninja.JumpBack;
    }

    void OnDestroy()
    {
        if (outputDevice != null && outputDevice.IsOpen)
        {
            //      outputDevice.SilenceAllNotes();
            //      outputDevice.Close();
        }
    }

    void OnDisable()
    {
        //if (outputDevice != null && outputDevice.IsOpen) outputDevice.Close();
    }





    public void minimumX(float x)
    {
        minX = x;
        return;
    }
    public void maximumX(float x)
    {
        maxX = x;
        return;
    }
    public void minimumY(float y)
    {
        minY = y;
        return;
    }
    public void maximumY(float y)
    {
        maxY = y;
        return;
    }

}