﻿using UnityEngine;
using System.Collections;

public class uihide : MonoBehaviour {

    private bool GUIEnabled;
    public GameObject menu;
    public GameObject second;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
   
        if (Input.GetKeyDown("escape"))
        {
            GUIEnabled = !GUIEnabled;
            menu.SetActive(GUIEnabled);
            second.SetActive(GUIEnabled);
        }
    }
}
