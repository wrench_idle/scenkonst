﻿using UnityEngine;
using System.Collections;


public class PlayerDetectorController : MonoBehaviour 
{
	public float userLostMaxTime = 2f;

	private KinectRecorderPlayer saverPlayer;
	private KinectInterop.SensorData sensorData;
	private KinectInterop.BodyFrameData bodyFrame;
	private Matrix4x4 kinectToWorld;

	private float lastUserTime = -20f;
    private float waitingTime = 0f;
    public float waitTime = 20f;

    private float demoTimer = 0;
    public bool bUserFound = false;

    public GameObject maxUserText;



    void Start()
	{
		saverPlayer = KinectRecorderPlayer.Instance;

		//sensorData = KinectManager.Instance.GetSensorData();
		//kinectToWorld = KinectManager.Instance.GetKinectToWorldMatrix();
		//bodyFrame = new KinectInterop.BodyFrameData(sensorData.bodyCount, KinectInterop.Constants.MaxJointCount);
	}

	void Update () 
	{
		if (!saverPlayer)
			return;
		
		//bool bPlayerActive = saverPlayer.IsPlaying();

		//if (bPlayerActive) 
		//{
		//	if (KinectInterop.PollBodyFrame (sensorData, ref bodyFrame, ref kinectToWorld, false)) 
		//	{
		//		for (int i = 0; i < sensorData.bodyCount; i++) 
		//		{
		//			if (bodyFrame.bodyData [i].bIsTracked != 0) 
		//			{
		//				lastUserTime = Time.realtimeSinceStartup;
		//				break;
		//			}
		//		}

		//		lock (sensorData.bodyFrameLock) 
		//		{
		//			sensorData.bodyFrameReady = false;
		//		}
		//	}
  //          waitingTime = Time.realtimeSinceStartup;
  //          demoTimer = 0;
		//} 
		//else 
		//{
            if (KinectManager.Instance.GetUsersCount() > 0 && demoTimer > 10)
            {
                lastUserTime = Time.realtimeSinceStartup;
                // Debug.Log("found");
                waitingTime = Time.realtimeSinceStartup;
            }
            demoTimer++;

       // }

        bUserFound = (Time.realtimeSinceStartup - lastUserTime) < userLostMaxTime;
        
		//if(!bPlayerActive && !bUserFound) 
		//{
  //          if ((Time.realtimeSinceStartup - waitingTime) > waitTime)
		//	saverPlayer.StartPlaying();
		//}
		//else if(bPlayerActive && bUserFound)
		//{
		//	saverPlayer.StopRecordingOrPlaying();
		//	KinectManager.Instance.ClearKinectUsers();
		//}

        if (KinectManager.Instance.GetUsersCount() > 3)
        {
           maxUserText.SetActive(true);
        } else
        {
           maxUserText.SetActive(false);
        }

    }

}
